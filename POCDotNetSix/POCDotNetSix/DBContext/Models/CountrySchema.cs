﻿using System.ComponentModel.DataAnnotations;

namespace POCDotNetSix.DBContext.Models
{
    public class CountrySchema
    {
        [Key]
        public int CountryId { get; set; }
        public Guid? CountryGuid { get; set; }
        public string? CountryName { get; set; }
        public DateTime? Date { get; set; }
        public int TemperatureC { get; set; }

    }
}
