﻿using POCDotNetSix.DBContext.Models;

namespace POCDotNetSix.Services.CountryProcess
{
    public interface ICountryProcess
    {
        Task<CountrySchema> CreateCountry(CountrySchemaCreate req);
        Task DeleteCountry(Guid? CountryGuid);
        Task<CountrySchema> UpdateCountry(Guid? CountryGuid, CountrySchemaUpdate weatherForecast);
        Task<CountrySchema[]> InquiryCountry();
        Task<CountrySchema> InquiryCountryWithId(Guid? CountryGuid);
    }
}
