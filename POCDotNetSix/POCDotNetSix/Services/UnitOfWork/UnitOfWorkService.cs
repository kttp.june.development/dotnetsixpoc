﻿using Microsoft.EntityFrameworkCore;
using POCDotNetSix.DBContext;
using System.Linq.Expressions;

namespace POCDotNetSix.Services.UnitOfWork
{
    public class UnitOfWorkService : IUnitOfWorkService
    {
        private readonly WeatherDbContext _dbContext;
        public UnitOfWorkService(WeatherDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public WeatherDbContext GetDbContext()
        {
            return _dbContext;
        }       
        public void Add<T>(T entity) where T : class
        {
            _dbContext.Set<T>().Add(entity);
        }

        public void Update<T>(T entity) where T : class
        {
            _dbContext.Set<T>().Update(entity);
        }

        public void Remove<T>(T entity) where T : class
        {
            _dbContext.Set<T>().Remove(entity);
        }

        public Task<T> GetAsync<T>(Expression<Func<T, bool>> expression) where T : class
        {
            return _dbContext.Set<T>().FirstOrDefaultAsync(expression);
        }

        public Task<T> GetNoTrackingAsync<T>(Expression<Func<T, bool>> expression) where T : class
        {
            return _dbContext.Set<T>().AsNoTracking().FirstOrDefaultAsync(expression);
        }

        public async Task<IEnumerable<T>> GetList<T>(Expression<Func<T, bool>> expression) where T : class
        {
            return await _dbContext.Set<T>().Where(expression).ToListAsync().ConfigureAwait(false);
        }

        public Task<T[]> GetArrayAsync<T>(Expression<Func<T, bool>> expression) where T : class
        {
            return _dbContext.Set<T>().Where(expression).ToArrayAsync();
        }

        public Task<int> CommitAsync()
        {
            return _dbContext.SaveChangesAsync();
        }
    }
}
