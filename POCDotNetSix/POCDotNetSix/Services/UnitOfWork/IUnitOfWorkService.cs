﻿using POCDotNetSix.DBContext;
using System.Linq.Expressions;

namespace POCDotNetSix.Services.UnitOfWork
{
    public interface IUnitOfWorkService
    {
        WeatherDbContext GetDbContext();
        void Add<T>(T entity) where T : class;
        void Update<T>(T entity) where T : class;
        void Remove<T>(T entity) where T : class;
        Task<T> GetAsync<T>(Expression<Func<T, bool>> expression) where T : class;
        Task<T> GetNoTrackingAsync<T>(Expression<Func<T, bool>> expression) where T : class;
        Task<IEnumerable<T>> GetList<T>(Expression<Func<T, bool>> expression) where T : class;
        Task<T[]> GetArrayAsync<T>(Expression<Func<T, bool>> expression) where T : class;
        Task<int> CommitAsync();
    }
}
