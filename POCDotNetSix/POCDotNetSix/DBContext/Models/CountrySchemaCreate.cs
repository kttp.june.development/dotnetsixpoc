﻿namespace POCDotNetSix.DBContext.Models
{
    public class CountrySchemaCreate
    {        
        public string? CountryName { get; set; }
        public DateTime? Date { get; set; }
    }
}
