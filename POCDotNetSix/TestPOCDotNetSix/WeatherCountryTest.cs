using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Moq;
using Moq.EntityFrameworkCore;
using Newtonsoft.Json;
using POCDotNetSix.Controllers;
using POCDotNetSix.DBContext;
using POCDotNetSix.DBContext.Models;
using POCDotNetSix.Services.CountryProcess;
using POCDotNetSix.Services.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using static POCDotNetSix.Helper.HttpActionHelper;

namespace TestPOCDotNetSix
{
    public class WeatherCountryTest
    {
        [Fact]
        public async Task InquiryCountry()
        {
            // Arrange
            var Context = InMemoryBloggingControllerTest();

            var unitOfWork = new UnitOfWorkService(Context);

            var countryProcess = new CountryProcess(unitOfWork);

            var WeatherCountryController = new WeatherCountry(countryProcess);

            // Act
            var result = await WeatherCountryController.InquiryCountry();
            var result_Success = result as HttpActionResult;
            var resultdata = (HttpActionResultData)result_Success._data;

            // Assert
            var actual_string = JsonConvert.SerializeObject(resultdata.Result);
            var expect_string = JsonConvert.SerializeObject(new List<CountrySchema>()
            {
               new CountrySchema(){ CountryId = 1, CountryGuid = Guid.NewGuid(), CountryName = "Thailand", Date=DateTime.Now, TemperatureC = 25 },
               new CountrySchema(){ CountryId = 2, CountryGuid = Guid.NewGuid(), CountryName = "China", Date=DateTime.Now, TemperatureC = 35 },
               new CountrySchema(){ CountryId = 3, CountryGuid = Guid.NewGuid(), CountryName = "India", Date=DateTime.Now, TemperatureC = 29 },
               new CountrySchema(){ CountryId = 4, CountryGuid = Guid.NewGuid(), CountryName = "Indonesia", Date=DateTime.Now, TemperatureC = 25 },
               new CountrySchema(){ CountryId = 5, CountryGuid = Guid.NewGuid(), CountryName = "Pakistan", Date=DateTime.Now, TemperatureC = 1 },
            });

            var actual = JsonConvert.DeserializeObject<List<CountrySchema>>(actual_string);
            var expect = JsonConvert.DeserializeObject<List<CountrySchema>>(expect_string);
            
            Assert.Equal(200, resultdata.Status);
            Assert.Equal(actual.Count(), expect.Count());
        }

        public WeatherDbContext InMemoryBloggingControllerTest()
        {
            var _contextOptions = new DbContextOptionsBuilder<WeatherDbContext>()
                .UseInMemoryDatabase("Inmem")
                .ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                .Options;

            var context = new WeatherDbContext(_contextOptions);

            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            var countrySchemas = new List<CountrySchema>()
            {
               new CountrySchema(){ CountryId = 1, CountryGuid = Guid.NewGuid(), CountryName = "Thailand", Date=DateTime.Now, TemperatureC = 25 },
               new CountrySchema(){ CountryId = 2, CountryGuid = Guid.NewGuid(), CountryName = "China", Date=DateTime.Now, TemperatureC = 35 },
               new CountrySchema(){ CountryId = 3, CountryGuid = Guid.NewGuid(), CountryName = "India", Date=DateTime.Now, TemperatureC = 29 },
               new CountrySchema(){ CountryId = 4, CountryGuid = Guid.NewGuid(), CountryName = "Indonesia", Date=DateTime.Now, TemperatureC = 25 },
               new CountrySchema(){ CountryId = 5, CountryGuid = Guid.NewGuid(), CountryName = "Pakistan", Date=DateTime.Now, TemperatureC = 1 },
            };
            context.AddRange(countrySchemas);
            context.SaveChanges();

            return context;
        }
    }
}