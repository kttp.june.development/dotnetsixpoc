﻿using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace POCDotNetSix.Helper
{
    public static class HttpActionHelper
    {
        public static IActionResult CustomResult<T>(HttpStatusCode errorCodes, T data) where T : class
        {
            return new HttpActionResult((int)errorCodes, data);
        }
        public static IActionResult CustomResult(HttpStatusCode errorCodes)
        {
            return new HttpActionResult((int)errorCodes);
        }
        public class HttpActionResult : IActionResult
        {
            private readonly int _statuscode;
            public readonly object _data;

            public HttpActionResult(int statuscode, object data)
            {
                _statuscode = statuscode;
                _data = new HttpActionResultData { Status = statuscode, Result = data };
            }
            public HttpActionResult(int statuscode)
            {
                _statuscode = statuscode;
                _data = new HttpActionResultData { Status = statuscode};
            }
            public Task ExecuteResultAsync(ActionContext context)
            {
                var objectResult = new ObjectResult(_data)
                {
                    StatusCode = _statuscode
                };

                return objectResult.ExecuteResultAsync(context);
            }
        }

        public class HttpActionResultData
        {
            public int Status { get; set; }
            public object? Result { get; set; }
        }
    }
}
