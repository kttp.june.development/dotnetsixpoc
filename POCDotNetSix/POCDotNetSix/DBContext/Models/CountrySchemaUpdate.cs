﻿namespace POCDotNetSix.DBContext.Models
{
    public class CountrySchemaUpdate
    {
        public string? CountryName { get; set; }
        public DateTime? Date { get; set; }
    }
}
