﻿using Microsoft.EntityFrameworkCore;
using POCDotNetSix.DBContext;
using POCDotNetSix.DBContext.Models;
using POCDotNetSix.Services.UnitOfWork;

namespace POCDotNetSix.Services.CountryProcess
{
    public class CountryProcess : ICountryProcess
    {
        private readonly IUnitOfWorkService _unitOfWorkService;
        public CountryProcess(IUnitOfWorkService unitOfWorkService)
        {
            _unitOfWorkService = unitOfWorkService;
        }


        public async Task<CountrySchema> CreateCountry(CountrySchemaCreate req)
        {
            CountrySchema country = new CountrySchema()
            {
                CountryGuid = Guid.NewGuid(),
                CountryName = req.CountryName,
                Date = DateTime.Now,
                TemperatureC = Random.Shared.Next(-20, 55)
            };
            _unitOfWorkService.Add(country);
            await _unitOfWorkService.CommitAsync().ConfigureAwait(false);

            return country;
        }

        public async Task DeleteCountry(Guid? CountryGuid)
        {
            var res = await _unitOfWorkService.GetNoTrackingAsync<CountrySchema>(x => x.CountryGuid == CountryGuid).ConfigureAwait(false);
            if (res != null)
            {
                _unitOfWorkService.Remove(res);
                await _unitOfWorkService.CommitAsync().ConfigureAwait(false);
            }
        }

        public async Task<CountrySchema[]> InquiryCountry()
        {
            return await _unitOfWorkService.GetArrayAsync<CountrySchema>(x => x.CountryId != 0).ConfigureAwait(false);
        }

        public async Task<CountrySchema> InquiryCountryWithId(Guid? CountryGuid)
        {
            return await _unitOfWorkService.GetAsync<CountrySchema>(x => x.CountryGuid == CountryGuid).ConfigureAwait(false);
        }

        public async Task<CountrySchema> UpdateCountry(Guid? CountryGuid, CountrySchemaUpdate weatherForecast)
        {
            var res = await _unitOfWorkService.GetNoTrackingAsync<CountrySchema>(x => x.CountryGuid == CountryGuid).ConfigureAwait(false);
            if (res != null)
            {
                res.CountryName = weatherForecast.CountryName;
                res.Date = DateTime.Now;
                _unitOfWorkService.Update(res);
                await _unitOfWorkService.CommitAsync().ConfigureAwait(false);
            }
            return res ?? new CountrySchema();
        }

    }
}
