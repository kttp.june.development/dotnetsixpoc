﻿using Microsoft.AspNetCore.Mvc;
using POCDotNetSix.DBContext.Models;
using POCDotNetSix.Helper;
using POCDotNetSix.Services.CountryProcess;
using POCDotNetSix.Services.UnitOfWork;

namespace POCDotNetSix.Controllers
{
    [ApiController]
    [Route("Country")]
    public class WeatherCountry : ControllerBase
    {
        private readonly ICountryProcess _countryProcess;
        public WeatherCountry(ICountryProcess countryProcess)
        {
            _countryProcess = countryProcess;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> InquiryCountry()
        {
            var resp = await _countryProcess.InquiryCountry().ConfigureAwait(false);
            return HttpActionHelper.CustomResult(System.Net.HttpStatusCode.OK, resp);
        }

        [HttpGet("{CountryGuid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> InquiryCountryWithUid([FromRoute] Guid? CountryGuid)
        {
            var resp = await _countryProcess.InquiryCountryWithId(CountryGuid).ConfigureAwait(false);
            return HttpActionHelper.CustomResult(System.Net.HttpStatusCode.OK, resp);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateCountry([FromBody] CountrySchemaCreate countrySchemaCreate)
        {
            var resp = await _countryProcess.CreateCountry(countrySchemaCreate).ConfigureAwait(false);
            return HttpActionHelper.CustomResult(System.Net.HttpStatusCode.OK, resp);
        }

        [HttpPut("{CountryGuid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> UpdateCountry([FromRoute] Guid? CountryGuid,[FromBody] CountrySchemaUpdate update)
        {
            var resp = await _countryProcess.UpdateCountry(CountryGuid,update).ConfigureAwait(false);
            return HttpActionHelper.CustomResult(System.Net.HttpStatusCode.OK, resp);
        }

        [HttpDelete("{CountryGuid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteCountry([FromRoute] Guid? CountryGuid)
        {
            await _countryProcess.DeleteCountry(CountryGuid).ConfigureAwait(false);
            return HttpActionHelper.CustomResult(System.Net.HttpStatusCode.OK);
        }

    }
}
