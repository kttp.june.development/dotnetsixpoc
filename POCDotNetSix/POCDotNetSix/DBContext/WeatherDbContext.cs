﻿using Microsoft.EntityFrameworkCore;
using POCDotNetSix.DBContext.Models;

namespace POCDotNetSix.DBContext
{
    public class WeatherDbContext : DbContext
    {
        public WeatherDbContext(DbContextOptions<WeatherDbContext> options) : base(options){
        }

        public virtual DbSet<CountrySchema> CountrySchemas { get; set; }
    }
}
